# Predicting deforestation

### A random forest model to predict deforestation in Borneo, Indonesia

#### Team: serious_naughty_bat

#### Group members: Duyen Bui, Robin Valkenburg, Wiby Setya Permana, and Lennart Murk

## Introduction:

<font size="4"> Borneo is home to ancient rain forests which are at the risk of deforestation. To combat this risk, machine learning technology can be used to detect areas at a high risk of deforestation. This allows for timely intervention and targeted conservation efforts.

## Objective

Produce an output layer with the possible areas interested by deforestation events in the area of interest located in the Borneo, Indonesia

## Research questions:

<em> What indicators are important to predict deforestation in Borneo, Indonesia?

How does the random forest model predict deforestation?

How accurate is the random forest model at predicting deforestation in Borneo, Indonesia? </em>

## Workflow

![](RFM.png)


## Results

<iframe src="defor_map.html" width="1000" height="600" frameborder="0" style="border:0;" allowfullscreen aria-hidden="false" tabindex="0">

</iframe>
![](Rplot.png)

## Conclusion
The model tells us the importance of the factors that we used to predict. The importance tells us that in our model, out of the factors that we used, the Protected_area (0.08800396) has the most impact on the model.

Our random forest model learns from training data (78%). The random forest model will build decision trees, each of these trees is trained on a subset of the data. Binary classification is used to predict whether there is deforestation or not, this is based on historical alerts and other factors like roads, and villages. The model is tested using the remaining 22% of data to calculate the accuracy (61%), precision (81%), recall (63%) and F1-score (71%). As seen from these accuracy scores there is room for improvement.

## Discussion

In case more time was available, the model could be improved. Possible improvements are: increasing the amount of factors that the model use, increasing the resolution of data, do a more extensive cleaning of the data. In terms of the algorithms, the number of trees in the Random Forest model could be increased. Alternatively, other machine learning models could be explored.

### What have we learned?

The project provided us an opportunity to reinforce the knowledge we have learned during the tutorials which we haven't really understood for example the principles and elements of random forest model. Moreover, we have learned new packages such as leaflet and shiny to visualize the results in terms of interactive maps and graphs. Additionally, working on R languages allows us to have a better understanding of functions used in the tutorials and how to use them properly. In term of strategic approach, we learned to formulate strategies to tackle the project’s objectives and determine the most effective pathways towards accomplishing our goals in the given time frame.</font>
